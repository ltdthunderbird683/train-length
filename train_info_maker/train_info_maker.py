#coding:utf-8
from urllib.parse import urljoin 
from getvalue.GetValue import getval
        
class TrainInfo_constant:
    def __init__(self,
    businessType = '00',
    screenId = 'CP0120',
    inputResultCount = '3',
    inputUniqueDepartSt = '0',
    inputUniqueArriveSt = '0',
    inputTransferDepartStUnique1 = '0',
    inputTransferArriveStUnique1 = '0',
    inputTransferDepartStUnique2 = '0',
    inputTransferArriveStUnique2 = '0',
    inputTransferDepartStUnique3 = '0',
    inputTransferArriveStUnique3 = '0',
    #inputDepartStName = '福井'.encode('Shift_JIS'),
    #inputArriveStName = '金沢'.encode('Shift_JIS'),
    #inputDate = '20191216',
    #inputHour = '04',
    #inputMinute = '30',
    inputType = '0',
    inputSearchType = '1',
    inputTransferDepartStName1 = '',
    inputTransferArriveStName1 = '',
    inputTransferTrainType1 = '0001',
    inputTransferDepartStName2 = '',
    inputTransferArriveStName2 = '',
    inputTransferTrainType2 = '0001',
    inputTransferDepartStName3 = '',
    inputTransferArriveStName3 = '',
    inputTransferTrainType3 = '0001',
    inputSinkansenTrain = '0001',
    inputNotSinkansen = '1'
        ):
        self.businessType = businessType
        self.screenId = screenId
        self.inputResultCount = inputResultCount
        self.inputUniqueDepartSt = inputUniqueDepartSt
        self.inputUniqueArriveSt = inputUniqueArriveSt
        self.inputTransferDepartStUnique1 = inputTransferDepartStUnique1
        self.inputTransferArriveStUnique1 = inputTransferArriveStUnique1
        self.inputTransferDepartStUnique2 = inputTransferDepartStUnique2
        self.inputTransferArriveStUnique2 = inputTransferArriveStUnique2
        self.inputTransferDepartStUnique3 = inputTransferDepartStUnique3
        self.inputTransferArriveStUnique3 = inputTransferArriveStUnique3
        #self.inputDepartStName= inputDepartStName
        #self.inputArriveStName = inputArriveStName
        #self.inputDate = inputDate
        #self.inputHour = inputHour
        #self.inputMinute = inputMinute
        self.inputType = inputType
        self.inputSearchType = inputSearchType
        self.inputTransferDepartStName1 = inputTransferDepartStName1
        self.inputTransferArriveStName1 = inputTransferArriveStName1
        self.inputTransferTrainType1 = inputTransferTrainType1
        self.inputTransferDepartStName2 = inputTransferDepartStName2
        self.inputTransferArriveStName2 = inputTransferArriveStName2
        self.inputTransferTrainType2 = inputTransferTrainType2
        self.inputTransferDepartStName3 = inputTransferDepartStName3
        self.inputTransferArriveStName3 = inputTransferArriveStName3
        self.inputTransferTrainType3 = inputTransferTrainType3
        self.inputSinkansenTrain = inputSinkansenTrain
        self.inputNotSinkansen = inputNotSinkansen
        
class TrainInfo:
    def __init__(self,
    TrainInfo_instance,
    businessType,
    screenId,
    inputUniqueDepartSt,
    inputUniqueArriveSt,
    inputTransferDepartStUnique1,
    inputTransferArriveStUnique1,
    inputTransferDepartStUnique2,
    inputTransferArriveStUnique2,
    inputTransferDepartStUnique3,
    inputTransferArriveStUnique3,
    inputDepartStName,
    inputArriveStName,
    inputDate,
    inputHour,
    inputMinute):
        self.businessType = businessType
        self.screenId = screenId
        self.inputResultCount = TrainInfo_instance.inputResultCount
        self.inputUniqueDepartSt = inputUniqueDepartSt
        self.inputUniqueArriveSt = inputUniqueArriveSt
        self.inputTransferDepartStUnique1 = inputTransferDepartStUnique1
        self.inputTransferArriveStUnique1 = inputTransferArriveStUnique1
        self.inputTransferDepartStUnique2 = inputTransferDepartStUnique2
        self.inputTransferArriveStUnique2 = inputTransferArriveStUnique2
        self.inputTransferDepartStUnique3 = inputTransferDepartStUnique3
        self.inputTransferArriveStUnique3 = inputTransferArriveStUnique3
        self.inputDepartStName= inputDepartStName.encode('Shift_JIS')
        self.inputArriveStName = inputArriveStName.encode('Shift_JIS')
        self.inputDate = inputDate
        self.inputHour = inputHour
        self.inputMinute = inputMinute
        self.inputType = TrainInfo_instance.inputType
        self.inputSearchType = TrainInfo_instance.inputSearchType
        self.inputTransferDepartStName1 = TrainInfo_instance.inputTransferDepartStName1
        self.inputTransferArriveStName1 = TrainInfo_instance.inputTransferArriveStName1
        self.inputTransferTrainType1 = TrainInfo_instance.inputTransferTrainType1
        self.inputTransferDepartStName2 = TrainInfo_instance.inputTransferDepartStName2
        self.inputTransferArriveStName2 = TrainInfo_instance.inputTransferArriveStName2
        self.inputTransferTrainType2 = TrainInfo_instance.inputTransferTrainType2
        self.inputTransferDepartStName3 = TrainInfo_instance.inputTransferDepartStName3
        self.inputTransferArriveStName3 = TrainInfo_instance.inputTransferArriveStName3
        self.inputTransferTrainType3 = TrainInfo_instance.inputTransferTrainType3
        self.inputSinkansenTrain = TrainInfo_instance.inputSinkansenTrain
        self.inputNotSinkansen = TrainInfo_instance.inputNotSinkansen
        
    def getDict(self):
        train_info_dict = {
        'businessType':self.businessType,
        'screenId':self.screenId,
        'inputResultCount':self.inputResultCount,
        'inputUniqueDepartSt':self.inputUniqueDepartSt,
        'inputUniqueArriveSt':self.inputUniqueArriveSt,
        'inputTransferDepartStUnique1':self.inputTransferDepartStUnique1,
        'inputTransferArriveStUnique1':self.inputTransferArriveStUnique1,
        'inputTransferDepartStUnique2':self.inputTransferDepartStUnique2,
        'inputTransferArriveStUnique2':self.inputTransferArriveStUnique2,
        'inputTransferDepartStUnique3':self.inputTransferDepartStUnique3,
        'inputTransferArriveStUnique3':self.inputTransferArriveStUnique3,
        'inputDepartStName':self.inputDepartStName,
        'inputArriveStName':self.inputArriveStName,
        'inputDate':self.inputDate,
        'inputHour':self.inputHour,
        'inputMinute':self.inputMinute,
        'inputType':self.inputType,
        'inputSearchType':self.inputSearchType,
        'inputTransferDepartStName1':self.inputTransferDepartStName1,
        'inputTransferArriveStName1':self.inputTransferArriveStName1,
        'inputTransferTrainType1':self.inputTransferTrainType1,
        'inputTransferDepartStName2':self.inputTransferDepartStName2,
        'inputTransferArriveStName2':self.inputTransferArriveStName2,
        'inputTransferTrainType2':self.inputTransferTrainType2,
        'inputTransferDepartStName3':self.inputTransferDepartStName3,
        'inputTransferArriveStName3':self.inputTransferArriveStName3,
        'inputTransferTrainType3':self.inputTransferTrainType3,
        'inputSinkansenTrain':self.inputSinkansenTrain,
        'inputNotSinkansen':self.inputNotSinkansen
        }
        return train_info_dict
        
    def getDict_check(self):
        train_info_dict2 = {
        'businessType':self.businessType,
        'screenId':self.screenId,
        'inputResultCount':self.inputResultCount,
        'inputUniqueDepartSt':self.inputUniqueDepartSt,
        'inputUniqueArriveSt':self.inputUniqueArriveSt,
        'inputTransferDepartStUnique1':self.inputTransferDepartStUnique1,
        'inputTransferArriveStUnique1':self.inputTransferArriveStUnique1,
        'inputTransferDepartStUnique2':self.inputTransferDepartStUnique2,
        'inputTransferArriveStUnique2':self.inputTransferArriveStUnique2,
        'inputTransferDepartStUnique3':self.inputTransferDepartStUnique3,
        'inputTransferArriveStUnique3':self.inputTransferArriveStUnique3,
        'inputDepartStName':self.inputDepartStName,
        'inputArriveStName':self.inputArriveStName,
        'inputDate':self.inputDate,
        'inputHour':self.inputHour,
        'inputMinute':self.inputMinute,
        'inputType':self.inputType,
        'inputSearchType':self.inputSearchType,
        'inputSinkansenTrain':self.inputSinkansenTrain,
        'inputNotSinkansen':self.inputNotSinkansen
        }
        return train_info_dict2
        
        
class DA_Station:
    def __init__(self,Dst='大阪',Ast='金沢'):
        self.Dst = Dst
        self.Ast = Ast
    
def search_DA_Station_value_from_webpage(soup): 
        inputDepartStName_tag = soup.find('select',{'name':'inputDepartStName'})
        inputDepartStName_tag_selected = inputDepartStName_tag.find('option',{'selected':'selected'}).contents[0]
        inputDepartStName = inputDepartStName_tag_selected
        inputArriveStName_tag = soup.find('select',{'name':'inputArriveStName'})
        inputArriveStName_tag_selected = inputArriveStName_tag.find('option',{'selected':'selected'}).contents[0]
        inputArriveStName = inputArriveStName_tag_selected
        DstAst_check = DA_Station(inputDepartStName,inputArriveStName)
        return DstAst_check
        

def getval_of_TrainInfo(train_info_constant,formMain,url_base,dstast,date,inputHour,inputMinute):
    formMain_url = urljoin(url_base,formMain['action'])
    businessType = getval(formMain,'input','businessType','value')
    screenId = getval(formMain,'input','screenId','value')
    inputUniqueDepartSt = getval(formMain,'input','inputUniqueDepartSt','value')
    inputUniqueArriveSt = getval(formMain,'input','inputUniqueArriveSt','value')
    inputTransferDepartStUnique1 = getval(formMain,'input','inputTransferDepartStUnique1','value')
    inputTransferArriveStUnique1 = getval(formMain,'input','inputTransferArriveStUnique1','value')
    inputTransferDepartStUnique2 = getval(formMain,'input','inputTransferDepartStUnique2','value')
    inputTransferArriveStUnique2 = getval(formMain,'input','inputTransferArriveStUnique2','value')
    inputTransferDepartStUnique3 = getval(formMain,'input','inputTransferDepartStUnique3','value')
    inputTransferArriveStUnique3 = getval(formMain,'input','inputTransferArriveStUnique3','value')
    


    train_info = TrainInfo(train_info_constant,
    businessType,
    screenId,
    inputUniqueDepartSt,
    inputUniqueArriveSt,
    inputTransferDepartStUnique1,
    inputTransferArriveStUnique1,
    inputTransferDepartStUnique2,
    inputTransferArriveStUnique2,
    inputTransferDepartStUnique3,
    inputTransferArriveStUnique3,
    dstast.Dst,
    dstast.Ast,
    date,
    inputHour,
    inputMinute)
    
    return formMain_url,train_info