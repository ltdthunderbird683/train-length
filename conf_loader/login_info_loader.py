from typing import Tuple


def id_and_password_loader(file_path: str) -> Tuple[str,str]:
    f = open(file_path,'r')
    lines = f.readlines()
    user_id = lines[0].strip()
    pw = lines[1].strip()
    f.close()
    return user_id, pw
