#coding:utf-8

import time
import requests
import sys
from bs4 import BeautifulSoup
from urllib.parse import urljoin 
from typing import Tuple,List,Dict
from conf_loader.login_info_loader import id_and_password_loader
from TimeModify.minutes_modify import MinuteModify
from getvalue.GetValue import getval
from logininfo.logininfo import get_login_info
from go_to_reservation.reservationPage import go_to_reservation

    

    
session = requests.session()
dst = sys.argv[1]
ast = sys.argv[2]
date = sys.argv[3]


ID,PW = id_and_password_loader('idpw.txt')

url_login,login_info = get_login_info(PW,ID,session)

res_after_login = session.post(url_login, data=login_info)

soup_after_login = BeautifulSoup(res_after_login.content,'html.parser')
iframe = soup_after_login.select('iframe')
iframe1 = iframe[0]
url_iframe1 = 'https://clubj.jr-odekake.net/shared/pc/'+iframe1['src']

#iframe内のurlをget(これでcookieが取得できる)
res_iframe1 = session.get(url_iframe1)
soup_iframe1 = BeautifulSoup(res_iframe1.content,'html.parser')
p_url = getval(soup_iframe1,'input','p_url01','value')

iframe2 = iframe[1]
url_iframe2 = iframe2['src']
res_iframe2=session.get(url_iframe2)


#トップページurlをget
res_top=session.get(p_url)
url_base = 'https://e5489.jr-odekake.net'



inputHour = '04'
inputMinute = '30'
zero_count = 0

#####################以下ループ######################
while True:
    url_reserve,info_entry = go_to_reservation(res_top)
    res = session.post(url_reserve,data = info_entry)
    soup = BeautifulSoup(res.content,'html.parser')
    
    #列車一覧へ遷移
    formMain = soup.find('form',{'name':'formMain'})
    formMain_url = urljoin(url_base,formMain['action'])
    businessType = getval(formMain,'input','businessType','value')
    screenId = getval(formMain,'input','screenId','value')
    inputResultCount = getval(formMain,'input','inputResultCount','value')
    inputUniqueDepartSt = getval(formMain,'input','inputUniqueDepartSt','value')
    inputUniqueArriveSt = getval(formMain,'input','inputUniqueArriveSt','value')
    inputTransferDepartStUnique1 = getval(formMain,'input','inputTransferDepartStUnique1','value')
    inputTransferArriveStUnique1 = getval(formMain,'input','inputTransferArriveStUnique1','value')
    inputTransferDepartStUnique2 = getval(formMain,'input','inputTransferDepartStUnique2','value')
    inputTransferArriveStUnique2 = getval(formMain,'input','inputTransferArriveStUnique2','value')
    inputTransferDepartStUnique3 = getval(formMain,'input','inputTransferDepartStUnique3','value')
    inputTransferArriveStUnique3 = getval(formMain,'input','inputTransferArriveStUnique3','value')
    inputDepartStName= dst.encode('Shift_JIS')
    inputArriveStName = ast.encode('Shift_JIS')
    inputDate = date
    inputType = '0'#出発
    inputSearchType = '1'#1度も乗り換えしない
    inputTransferTrainType1 = '0001'
    inputTransferTrainType2 = '0001'
    inputTransferTrainType3 = '0001'
    inputSinkansenTrain = '0001'#新幹線：指定しない
    inputNotSinkansen = '1'#特急快速のみを利用
    
    train_info = {
        'businessType':businessType,
        'screenId':screenId,
        'inputResultCount':inputResultCount,
        'inputUniqueDepartSt':inputUniqueDepartSt,
        'inputUniqueArriveSt':inputUniqueArriveSt,
        'inputTransferDepartStUnique1':inputTransferDepartStUnique1,
        'inputTransferArriveStUnique1':inputTransferArriveStUnique1,
        'inputTransferDepartStUnique2':inputTransferDepartStUnique2,
        'inputTransferArriveStUnique2':inputTransferArriveStUnique2,
        'inputTransferDepartStUnique3':inputTransferDepartStUnique3,
        'inputTransferArriveStUnique3':inputTransferArriveStUnique3,
        'inputDepartStName':inputDepartStName,
        'inputArriveStName':inputArriveStName,
        'inputDate':inputDate,
        'inputHour':inputHour,
        'inputMinute':inputMinute,
        'inputType':inputType,
        'inputSearchType':inputSearchType,
        'inputTransferDepartStName1':'',
        'inputTransferArriveStName1':'',
        'inputTransferTrainType1':inputTransferTrainType1,
        'inputTransferDepartStName2':'',
        'inputTransferArriveStName2':'',
        'inputTransferTrainType2':inputTransferTrainType2,
        'inputTransferDepartStName3':'',
        'inputTransferArriveStName3':'',
        'inputTransferTrainType3':inputTransferTrainType3,
        'inputSinkansenTrain':inputSinkansenTrain,
        'inputNotSinkansen':inputNotSinkansen
    }
    
    res = session.post(formMain_url,data=train_info)
    soup = BeautifulSoup(res.content,'html.parser')
    
    
    error = soup.find('div',class_='error-message')
    if error != None:
        break
    
    ####################駅名確認がある場合########################
    step = soup.find('ol',class_='step-list')
    selected_step = step.find('li',class_='selected')
    selected_step = selected_step.text
    if selected_step == '条件入力':
        formMain = soup.find('form',{'name':'formMain'})
        formMain_url = formMain['action']
        formMain_url = urljoin(url_base,formMain_url)
        businessType = getval(formMain,'input','businessType','value')
        screenId = getval(formMain,'input','screenId','value')
        inputResultCount = getval(formMain,'input','inputResultCount','value')
        inputUniqueDepartSt = getval(formMain,'input','inputUniqueDepartSt','value')
        inputUniqueArriveSt = getval(formMain,'input','inputUniqueArriveSt','value')
        inputTransferDepartStUnique1 = getval(formMain,'input','inputTransferDepartStUnique1','value')
        inputTransferArriveStUnique1 = getval(formMain,'input','inputTransferArriveStUnique1','value')
        inputTransferDepartStUnique2 = getval(formMain,'input','inputTransferDepartStUnique2','value')
        inputTransferArriveStUnique2 = getval(formMain,'input','inputTransferArriveStUnique2','value')
        inputTransferDepartStUnique3 = getval(formMain,'input','inputTransferDepartStUnique3','value')
        inputTransferArriveStUnique3 = getval(formMain,'input','inputTransferArriveStUnique3','value')
        inputDepartStName = soup.find('select',{'name':'inputDepartStName'})
        inputDepartStName = inputDepartStName.find('option',{'selected':'selected'}).contents[0]
        inputDepartStName = inputDepartStName.encode('Shift_JIS')
        inputArriveStName = soup.find('select',{'name':'inputArriveStName'})
        inputArriveStName = inputArriveStName.find('option',{'selected':'selected'}).contents[0]
        inputArriveStName = inputArriveStName.encode('Shift_JIS')
        inputDate = date
        inputType = '0'#出発
        inputSearchType = '1'#1度も乗り換えしない
        inputSinkansenTrain = '0001'#新幹線：指定しない
        inputNotSinkansen = '1'#特急快速のみを利用
        
        train_info = {
            'businessType':businessType,
            'screenId':screenId,
            'inputResultCount':inputResultCount,
            'inputUniqueDepartSt':inputUniqueDepartSt,
            'inputUniqueArriveSt':inputUniqueArriveSt,
            'inputTransferDepartStUnique1':inputTransferDepartStUnique1,
            'inputTransferArriveStUnique1':inputTransferArriveStUnique1,
            'inputTransferDepartStUnique2':inputTransferDepartStUnique2,
            'inputTransferArriveStUnique2':inputTransferArriveStUnique2,
            'inputTransferDepartStUnique3':inputTransferDepartStUnique3,
            'inputTransferArriveStUnique3':inputTransferArriveStUnique3,
            'inputDepartStName':inputDepartStName,
            'inputArriveStName':inputArriveStName,
            'inputDate':inputDate,
            'inputHour':inputHour,
            'inputMinute':inputMinute,
            'inputType':inputType,
            'inputSearchType':inputSearchType,
            'inputSinkansenTrain':inputSinkansenTrain,
            'inputNotSinkansen':inputNotSinkansen
        }
    
        res = session.post(formMain_url,data=train_info)
        soup = BeautifulSoup(res.content,'html.parser')
        
        error = soup.find('div',class_='error-message')
        if error != None:
            break
        
    ############################################
    
    
    #検索した時刻より後で一番早く発車する特急を探す
    
    expressGet = 1
    while expressGet > 0:
    
    
        trainType_list=[]
        
        try:
            formMain1 = soup.find('form',{'name':'formMain1'})
            trainType = formMain1.find('div',class_='route-train-list__train-name').get('class')
            trainType_list.append(trainType[1])
        except AttributeError:
            break
        
        try:
            formMain2 = soup.find('form',{'name':'formMain2'})
            trainType = formMain2.find('div',class_='route-train-list__train-name').get('class')
            trainType_list.append(trainType[1])
        except AttributeError:
            break
            
        try:
            formMain3 = soup.find('form',{'name':'formMain3'})
            trainType = formMain3.find('div',class_='route-train-list__train-name').get('class')
            trainType_list.append(trainType[1])
        except AttributeError:
            break
        
        if trainType_list == []:
            break
        
        
        routeNumber = 0
        for i in range(len(trainType_list)):
            routeNumber += 1
            if trainType_list[i] == 'express':
                expressGet = 0
                break
            
            if routeNumber == 3:#次のページへ遷移
                formAfterResearch = soup.find('form',{'name':'formAfterReSearch'})
                next_url = formAfterResearch['action']
                next_url = urljoin(url_base,next_url)
                screenId = getval(formAfterResearch,'input','screenId','value')
                businessType = getval(formAfterResearch,'input','businessType','value')
                inputReSearchType = getval(formAfterResearch,'input','inputReSearchType','value')
                inputReSearchTime = getval(formAfterResearch,'input','inputReSearchTime','value')
                
                next_info = {
                    'screenId':screenId,
                    'businessType':businessType,
                    'inputReSearchType':inputReSearchType,
                    'inputReSearchTime':inputReSearchTime
                }
                
                res = session.post(next_url,data=next_info)
                soup = BeautifulSoup(res.content,'html.parser')
        
    #選んだ経路番号によってpostする変数名や値が変わるので調整
    choiceNo = '0'+str(routeNumber)
    sewerage = 'sewerage'+str(routeNumber)+'_1'
    tagName = 'formMain'+str(routeNumber)
    
    formMain = soup.find('form',{'name':tagName})
    if formMain == None:
        break
    #列車名を取得
    TrainName = formMain.find('div', class_ = 'route-train-list__train-name')
    TrainName = TrainName.p.contents[0]
    
    #列車の発車時間の取得
    TrainTime = formMain.find('div',class_='route-train-list__header-inner')
    TrainTime = TrainTime.find('p',class_='route-train-list__time').contents[0]
    TrainTime = TrainTime.replace(' ','').strip('発').split(':')
    hour = TrainTime[0]
    minute = TrainTime[1]
    nextTimeLst = MinuteModify(hour,minute)
    
    #切符選択ページへ遷移
    url_formMain1 = formMain['action']
    url_formMain1 = urljoin(url_base,url_formMain1)
    
    
    messageNo = getval(formMain,'input','messageNo','value')
    screenId = getval(formMain,'input','screenId','value')
    businessType = getval(formMain,'input','businessType','value')
    
    formMain1_info = {
        'messageNo':messageNo,
        'choiceNo':choiceNo,# 経路nを選ぶと値は0nになる
        'screenId':screenId,
        'businessType':businessType,
        sewerage:'3010000',#sewarage n_1 nは選んだ経路の番号
        'opLogTrainName1':TrainName.encode('Shift_JIS'),
        'opLogSewerage1':'普通車指定席'.encode('Shift_JIS'),
        'opLogEquipment1':'普通'.encode('Shift_JIS')
    }
    
    res = session.post(url_formMain1,data = formMain1_info)
    soup = BeautifulSoup(res.content,'html.parser')
    
    #指定席が満席、指定席の設定がないとerrorページへ飛ぶのでその対策
    error = soup.find('div',class_='error-message')
    if error != None:
        print(TrainName)
        print('不明')
        formTop = soup.find('form',{'name':'formTop'})
        url_top = formTop['action']
        url_top = urljoin(url_base,url_top)
        businessType = getval(formTop,'input','businessType','value')
        screenId = getval(formTop,'input','screenId','value')
        top_info = {'businessType':businessType,'screenId':screenId}
        res = session.post(url_top,data=top_info)
        inputHour = nextTimeLst[0]
        inputMinute = nextTimeLst[1]
        
        continue
    
    seat_url = 'e5489/cspc/CBNumberSeatPC'
    seat_url = urljoin(url_base,seat_url)
    
    #大人料金、子供料金を取得
    AdultMoney = soup.find('strong',class_='ticket-type-table__total-price')
    AdultMoney = AdultMoney.contents[0]
    ChildMoney = AdultMoney.replace(",",'').strip('円')
    ChildMoney = float(ChildMoney)
    ChildMoney = int(ChildMoney/20)*10
    ChildMoney = str(float(ChildMoney/1000))
    ChildMoney = ChildMoney.replace('.',',')+'00円'
    
    
    #乗車券・人数選択ページへ遷移
    formCommon = soup.find('form',{'name':'formCommon'})
    businessType = getval(formCommon,'input','businessType','value')
    screenId = getval(formCommon,'input','screenId','value')
    messageNo = getval(formCommon,'input','messageNo','value')
    screenSequenceNo = getval(formCommon,'input','screenSequenceNo','value')
    choiceNo = getval(formCommon,'input','choiceNo','value')
    #serviceCd = getval(formCommon,'input','serviceCd','value')
    #inquiryNo = getval(formCommon,'input','inquiryNo','value')
    opLogTicketName = soup.find('a',href='#ticket-1').contents[0]
    opLogAdultsSummary = AdultMoney
    opLogChildsSummary = ChildMoney
    opLogAmountCoupon = AdultMoney.strip('円')+' '+'+'+' '+ChildMoney
    
    common_info = {
        'businessType':businessType,
        'screenId':screenId,
        'messageNo':messageNo,
        'screenSequenceNo':screenSequenceNo,
        'choiceNo':choiceNo,
        'serviceCd':'',#jsの引数
        'inquiryNo':'01',#通常の切符なら基本01でOK?
        'opLogTicketName':opLogTicketName.encode('Shift_JIS'),
        'opLogAdultsSummary':opLogAdultsSummary.encode('Shift_JIS'),
        'opLogChildsSummary':opLogChildsSummary.encode('Shift_JIS'),
        'opLogAmountCoupon':opLogAmountCoupon.encode('Shift_JIS')
    }
    
    
    res = session.post(seat_url,data=common_info)
    soup = BeautifulSoup(res.content,'html.parser')
        
        
    #座席確認ページへ遷移    
    formMain = soup.find('form',{'name':'formMain'})
    error = soup.find('div',class_='error-message')
    if error != None:
        print(TrainName+'\n'+'満席やで')
        inputHour = MinuteModify(nextTimeLst[0],nextTimeLst[1])[0]
        inputMinute = MinuteModify(nextTimeLst[0],nextTimeLst[1])[0]
        continue
    final_url = formMain['action']
    final_url = urljoin(url_base,final_url)
        
        
    businessType = getval(formMain,'input','businessType','value')
    screenId = getval(formMain,'input','screenId','value')
    messageNo = getval(formMain,'input','messageNo','value')
    choiceNo = getval(formMain,'input','choiceNo','value')
    discPlanKbn = getval(formMain,'input','discPlanKbn','value')
    branchTrainName1 = getval(formMain,'input','branchTrainName1','value')
    branchSewerageType1 = getval(formMain,'input','branchSewerageType1','value')
    branchSmokeType1 = getval(formMain,'input','branchSmokeType1','value')
        
    final_info = {
        'AdultsNum':'1',#大人の人数
        'ChildsNum':'0',#子どもの人数
        'seatPosition1':'21',#自分で選択
        'ticketType':'1',#片道乗車券
        'businessType':businessType,
        'screenId':screenId,
        'messageNo':messageNo,
        'choiceNo':choiceNo,
        'discPlanKbn':discPlanKbn,
        'branchTrainName1':branchTrainName1.encode('Shift_JIS'),
        'branchSewerageType1':branchSewerageType1,
        'branchSmokeType1':branchSmokeType1
        }
    
    res = session.post(final_url,data=final_info)
    soup = BeautifulSoup(res.content,'html.parser')
    ryousu_list = soup.select('.seat-info-car-navigation__number')
    ryousu = len(ryousu_list)
    #time.sleep(10)
    
    #topページへ戻る
    formTop = soup.find('form',{'name':'formTop'})
    url_top = formTop['action']
    url_top = urljoin(url_base,url_top)
    businessType = getval(formTop,'input','businessType','value')
    screenId = getval(formTop,'input','screenId','value')
    top_info = {'businessType':businessType,'screenId':screenId}
    res = session.post(url_top,data=top_info)
    
    #両数が０になるエラーが発生しがちなので、両数が０になったらやり直すように設定
    #サンライズなどの特殊な車両の場合、両数が０になってしまうので、
    #zero_errorが3回発生すれば次の特急の情報を取得するよう設定
    if ryousu == 0:
        #print('zero_error')
        zero_count += 1
        if zero_count == 3:
            zero_count = 0
            print(TrainName+'\n不明')
            inputHour = nextTimeLst[0]
            inputMinute = nextTimeLst[1]
            continue
        continue
    else:
        zero_count = 0
    
    print(TrainName)
    ryousu = str(ryousu)+'両'
    print(ryousu)
    
    #次の検索時刻を設定
    inputHour = nextTimeLst[0]
    inputMinute = nextTimeLst[1]
