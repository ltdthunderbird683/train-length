import time
import datetime
import requests
import sys
from bs4 import BeautifulSoup


train_name_dict = {
    'ThunderBird':'サンダーバード',
    'Shirasagi':'しらさぎ',
    'Kuroshio_hanwa':'くろしお　新大阪～和歌山',
    'Kuroshio_kinokuni':'くろしお　和歌山以南',
    'Yakumo':'やくも',
    'Shinano':'しなの',
    'Hamakaze':'はまかぜ'
    }

def getval(s,tag: str,name: str,element: str)->str:#s(oup)中のnameという名前のtagのelement(要素)の値を取得
    try:
        val = s.find(tag,{'name':name})
        val = val[element]
    except KeyError:
        val = ''
    return val

def toukou(train_name,up_or_down,date):
    year = str(date)[0:4]
    month = str(date)[4:6]
    day = str(date)[6:8]
    if month[0] == '0':
        month = month[1]
    if day[0] == '0':
        day = day[1]
    
    
    session = requests.session()
    res_before_login = session.get('https://www.hatena.ne.jp/login?location=%2F%2Fblog.hatena.ne.jp%2Fgo%3Fblog%3Dhttps%253A%252F%252Fhatenablog.com%252F')
    soup_before_login = BeautifulSoup(res_before_login.content,'html.parser')
    login_action_url = 'https://www.hatena.ne.jp/login'
    location_tag = soup_before_login.find('input',{'name':'location'})
    location = location_tag['value']
    login_info = {
        'name':'ltdthunderbird683',
        'password':'hokurikusenn683',
        'persistent':'1',
        'location':location
    }
    
    res_after_login = session.post(login_action_url,data=login_info)
    soup_after_login = BeautifulSoup(res_after_login.content,'html.parser')
    kochira_tag = soup_after_login.find('div',class_='note')
    kochira_tag_url = 'https:'+kochira_tag.find('a')['href']
    res_mypage = session.get(kochira_tag_url)
    soup_mypage = BeautifulSoup(res_mypage.content,'html.parser')
    res_dashbord = session.get('https://blog.hatena.ne.jp/')
    soup_dashbord = BeautifulSoup(res_dashbord.content,'html.parser')
    edit_url = soup_dashbord.find('a',class_='admin-menu-edit-btn')['href']
    res_edit_page = session.get(edit_url)
    soup_edit_page = BeautifulSoup(res_edit_page.content,'html.parser')
    edit_form = soup_edit_page.find('form',id='edit-form')
    toukou_url = edit_form['action']
    
    if up_or_down == 'up':
        direction = '上り'
    elif up_or_down == 'down':
        direction = '下り'
    title = str(year)+'年'+str(month)+'月'+str(day)+'日'+' '+str(train_name_dict[train_name])+' '+str(direction)
    file = 'ryousu/'+str(train_name)+'_'+str(up_or_down)+'.txt'
    f = open(file)
    body = ''
    for line in f:
        body = body + str(line)+'<br>'
    
    rkm = getval(edit_form,'input','rkm','value')
    rkc = getval(edit_form,'input','rkc','value')
    
    edit_form_data = {
        'title':title,
        'body':body,
        'preview-device':'pc',
        'entry':'',
        'syntax':'html',
        'editinplace':'0',
        'publication_type':'entry',
        'rkm':rkm,
        'rkc':rkc,
        'fotolife-layout':'single',
        'embed-confit-fotolife':'on',
        'datetime':'',
        'customurl':'',
        'category':train_name_dict[train_name],
        'ogimage':'',
        'og_description':'',
        'page_title':'',
        'og_title':'',
        'nico-videoformat':'player',
        'amazon-search-category':'',
        'amazon-search-layout-type':'list',
        'amazon-search-format':'detail',
        'itunes-media':'all',
        'pref':'',
        'gourmet-provider':'gnavi',
        'gourmet-format':'detail',
        'unescaped_body':body
    }
    
    res_toukou = session.post(toukou_url,data=edit_form_data)
    
    
date = sys.argv[1]
for train_name in train_name_dict:
    for up_or_down in ['up','down']:
        toukou(train_name,up_or_down,date)

