#coding:utf-8

import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin 

def getval(s,tag,name,element):#s(oup)中のnameという名前のtagのelement(要素)の値を取得
    try:
        val = s.find(tag,{'name':name})
        val = val[element]
    except KeyError:
        val = ''
    return val
    
def absurl(relurl):
    u = 'https://e5489.jr-odekake.net'+relurl
    return u
    
def MinuteModify(minu):
    if minu[0] == '0':
        mod_min = minu[1]
    
    mod_min = float(minu)
    mod_min = mod_min/5
    mod_min = int(mod_min)*5
    if mod_min<10:
        mod_min = '0'+str(mod_min)
    else:
        mod_min = str(mod_min)
    return mod_min
    
def test(resp):
    soup = BeautifulSoup(resp.content,'html.parser')
    print(soup)
        
    




ID = '360811563814'
PW = '14801480Ss'

session = requests.session()
res = session.get('https://e5489.jr-odekake.net/e5489/cspc/CBTopMenuPC?screenId=CP6100&businessType=10&FMLOGIN=1')

# 先にログインページにアクセスしてクッキーを保存する
url_login = 'https://clubj.jr-odekake.net/shared/pc/login2.do?JRSSID=0409&RTURL=https%3a%2f%2fwww.jr-odekake.net%2f&NTURL=https%3a%2f%2fe5489.jr-odekake.net%2fe5489%2fcspc%2fCBTopMenuPC'
res = session.get(url_login)
soup = BeautifulSoup(res.content,'html.parser')
url_login = soup.find('form',{'name':'login2Form'})
url_login = url_login['action']
url_login = 'https://clubj.jr-odekake.net/shared/pc/'+url_login

jrssid = soup.find('input',{'name':'JRSSID'})
jrssid = jrssid['value']
rturl = soup.find('input',{'name':'RTURL'})
rturl = rturl['value']
nturl = soup.find('input',{'name':'NTURL'})
nturl = nturl['value']
sbmtok = soup.find('input',{'name':'sbmtok'})
sbmtok = sbmtok['value']

login_info = {
    'id':ID,
    'password':PW,
    'JRSSID':jrssid,
    'RTURL':rturl,
    'NTURL':nturl,
    'LIVSID':'',
    'LIVVID':'',
    'sbmtok':sbmtok#文字コード大丈夫か?
}

res = session.post(url_login, data=login_info)
res.raise_for_status()

soup = BeautifulSoup(res.content,'html.parser')
iframe = soup.select('iframe')
iframe1 = iframe[0]
url_iframe1 = iframe1['src']
url_iframe1 = 'https://clubj.jr-odekake.net/shared/pc/'+url_iframe1
iframe2 = iframe[1]
url_iframe2 = iframe2['src']
#iframe内のurlをget(これでcookieが取得できた)

res=session.get(url_iframe1)
soup = BeautifulSoup(res.content,'html.parser')
p_url = soup.find('input',{'name':'p_url01'})
p_url = p_url['value']

res=session.get(url_iframe2)


#トップページurlをget
res=session.get(p_url)
url_base = 'https://e5489.jr-odekake.net'
soup = BeautifulSoup(res.content,'html.parser')



#予約ページへ遷移
url_reserve = getval(soup,'form','formTrainEntry','action')
url_reserve = urljoin(url_base,url_reserve)

formTrainEntry = soup.find('form',{'name':'formTrainEntry'})
businessType = getval(formTrainEntry,'input','businessType','value')
screenId = getval(formTrainEntry,'input','screenId','value')


info_entry = {'businessType':businessType,'screenId':screenId}
res = session.post(url_reserve,data = info_entry)
soup = BeautifulSoup(res.content,'html.parser')

#列車一覧へ遷移

formMain_url = getval(soup,'form','formMain','action')
formMain_url = urljoin(url_base,formMain_url)

formMain = soup.find('form',{'name':'formMain'})
businessType = getval(formMain,'input','businessType','value')
screenId = getval(formMain,'input','screenId','value')
inputResultCount = getval(formMain,'input','inputResultCount','value')
inputUniqueDepartSt = getval(formMain,'input','inputUniqueDepartSt','value')
inputUniqueArriveSt = getval(formMain,'input','inputUniqueArriveSt','value')
inputTransferDepartStUnique1 = getval(formMain,'input','inputTransferDepartStUnique1','value')
inputTransferArriveStUnique1 = getval(formMain,'input','inputTransferArriveStUnique1','value')
inputTransferDepartStUnique2 = getval(formMain,'input','inputTransferDepartStUnique2','value')
inputTransferArriveStUnique2 = getval(formMain,'input','inputTransferArriveStUnique2','value')
inputTransferDepartStUnique3 = getval(formMain,'input','inputTransferDepartStUnique3','value')
inputTransferArriveStUnique3 = getval(formMain,'input','inputTransferArriveStUnique3','value')
inputDepartStName='米原'.encode('Shift_JIS')
inputArriveStName = '金沢'.encode('Shift_JIS')
inputDate = '20191128'
inputHour = '04'
inputMinute = '30'
inputType = '0'#出発
inputSearchType = '1'#1度も乗り換えしない
inputTransferTrainType1 = '0001'
inputTransferTrainType2 = '0001'
inputTransferTrainType3 = '0001'
inputSinkansenTrain = '0001'#新幹線：指定しない
inputNotSinkansen = '1'#特急快速のみを利用

train_info = {
    'businessType':businessType,
    'screenId':screenId,
    'inputResultCount':inputResultCount,
    'inputUniqueDepartSt':inputUniqueDepartSt,
    'inputUniqueArriveSt':inputUniqueArriveSt,
    'inputTransferDepartStUnique1':inputTransferDepartStUnique1,
    'inputTransferArriveStUnique1':inputTransferArriveStUnique1,
    'inputTransferDepartStUnique2':inputTransferDepartStUnique2,
    'inputTransferArriveStUnique2':inputTransferArriveStUnique2,
    'inputTransferDepartStUnique3':inputTransferDepartStUnique3,
    'inputTransferArriveStUnique3':inputTransferArriveStUnique3,
    'inputDepartStName':inputDepartStName,
    'inputArriveStName':inputArriveStName,
    'inputDate':inputDate,
    'inputHour':inputHour,
    'inputMinute':inputMinute,
    'inputType':inputType,
    'inputSearchType':inputSearchType,
    'inputTransferDepartStName1':'',
    'inputTransferArriveStName1':'',
    'inputTransferTrainType1':inputTransferTrainType1,
    'inputTransferDepartStName2':'',
    'inputTransferArriveStName2':'',
    'inputTransferTrainType2':inputTransferTrainType2,
    'inputTransferDepartStName3':'',
    'inputTransferArriveStName3':'',
    'inputTransferTrainType3':inputTransferTrainType3,
    'inputSinkansenTrain':inputSinkansenTrain,
    'inputNotSinkansen':inputNotSinkansen
}

res = session.post(formMain_url,data=train_info)
soup = BeautifulSoup(res.content,'html.parser')

#列車名を取得
TrainName = soup.find('div', class_ = 'route-train-list__train-name')
TrainName = TrainName.p.contents[0]


url_formMain1 = getval(soup,'form','formMain1','action')
url_formMain1 = urljoin(url_base,url_formMain1)

formMain1 = soup.find('form',{'name':'formMain1'})
messageNo = getval(formMain1,'input','messageNo','value')
#choiceNo = getval(formMain1,'input','choiceNo','value')
screenId = getval(formMain1,'input','screenId','value')
businessType = getval(formMain1,'input','businessType','value')

formMain1_info = {
    'messageNo':messageNo,
    'choiceNo':'01',#jsの関数の引数
    'screenId':screenId,
    'businessType':businessType,
    'sewerage1_1':'3010000',
    'opLogTrainName1':TrainName.encode('Shift_JIS'),
    'opLogSewerage1':'普通車指定席'.encode('Shift_JIS'),
    'opLogEquipment1':'普通'.encode('Shift_JIS')
}

res = session.post(url_formMain1,data = formMain1_info)
soup = BeautifulSoup(res.content,'html.parser')
seat_url = 'e5489/cspc/CBNumberSeatPC'
seat_url = urljoin(url_base,seat_url)

#大人料金、子供料金を取得
AdultMoney = soup.find('strong',class_='ticket-type-table__total-price')
AdultMoney = AdultMoney.contents[0]
ChildMoney = AdultMoney.replace(",",'').strip('円')
ChildMoney = float(ChildMoney)
ChildMoney = int(ChildMoney/20)*10
ChildMoney = str(float(ChildMoney/1000))
ChildMoney = ChildMoney.replace('.',',')+'00円'



formCommon = soup.find('form',{'name':'formCommon'})
businessType = getval(formCommon,'input','businessType','value')
screenId = getval(formCommon,'input','screenId','value')
messageNo = getval(formCommon,'input','messageNo','value')
screenSequenceNo = getval(formCommon,'input','screenSequenceNo','value')
choiceNo = getval(formCommon,'input','choiceNo','value')
#serviceCd = getval(formCommon,'input','serviceCd','value')
#inquiryNo = getval(formCommon,'input','inquiryNo','value')
opLogTicketName = soup.find('a',href='#ticket-1').contents[0]
opLogAdultsSummary = AdultMoney
opLogChildsSummary = ChildMoney
opLogAmountCoupon = AdultMoney.strip('円')+' '+'+'+' '+ChildMoney

common_info = {
    'businessType':businessType,
    'screenId':screenId,
    'messageNo':messageNo,
    'screenSequenceNo':screenSequenceNo,
    'choiceNo':choiceNo,
    'serviceCd':'',#js
    'inquiryNo':'01',#js
    'opLogTicketName':opLogTicketName.encode('Shift_JIS'),
    'opLogAdultsSummary':opLogAdultsSummary.encode('Shift_JIS'),
    'opLogChildsSummary':opLogChildsSummary.encode('Shift_JIS'),
    'opLogAmountCoupon':opLogAmountCoupon.encode('Shift_JIS')
}


res = session.post(seat_url,data=common_info)
soup = BeautifulSoup(res.content,'html.parser')
    
    
    
formMain = soup.find('form',{'name':'formMain'})
final_url = formMain['action']
final_url = absurl(final_url)
    
    
businessType = getval(formMain,'input','businessType','value')
screenId = getval(formMain,'input','screenId','value')
messageNo = getval(formMain,'input','messageNo','value')
choiceNo = getval(formMain,'input','choiceNo','value')
discPlanKbn = getval(formMain,'input','discPlanKbn','value')#########
branchTrainName1 = getval(formMain,'input','branchTrainName1','value')
branchSewerageType1 = getval(formMain,'input','branchSewerageType1','value')
branchSmokeType1 = getval(formMain,'input','branchSmokeType1','value')
    
    
print(TrainName)
    
final_info = {
    'AdultsNum':'1',#大人の人数
    'ChildsNum':'0',#子どもの人数
    'seatPosition1':'21',#自分で選択
    'ticketType':'1',#片道乗車券
    'businessType':businessType,
    'screenId':screenId,
    'messageNo':messageNo,
    'choiceNo':choiceNo,
    'discPlanKbn':discPlanKbn,
    'branchTrainName1':branchTrainName1.encode('Shift_JIS'),
    'branchSewerageType1':branchSewerageType1,
    'branchSmokeType1':branchSmokeType1
    }

res = session.post(final_url,data=final_info)
soup = BeautifulSoup(res.content,'html.parser')
ryousu_list = soup.select('.seat-info-car-navigation__number')
ryousu = len(ryousu_list)
ryousu = str(ryousu)+'両'
print(ryousu)
    
    
    








