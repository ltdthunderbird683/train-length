#coding:utf-8
from typing import Tuple
def MinuteModify(hour :str,minutes :str)->Tuple[str,str]:
    if hour[0] == '0':
        intHour = int(hour[1])
    else:
        intHour = int(hour)
    
    if minutes[0] == '0':
        intMinutes = minutes[1]
    intMinutes = int(minutes)
    
    if intMinutes+1 == 60:
        nextIntMinutes = 0
        nextIntHour = intHour+1
    else:
        nextIntMinutes = intMinutes+1
        nextIntHour = intHour
        
    if 0<=nextIntMinutes<10:
        nextStrMinutes = '0'+str(nextIntMinutes)
    else:
        nextStrMinutes = str(nextIntMinutes)
    
    if 0<=nextIntHour<10:
        nextStrHour = '0'+str(nextIntHour)
    else:
        nextStrHour = str(nextIntHour)
        
    return (nextStrHour,nextStrMinutes)