#coding:utf-8
from typing import List
from bs4 import BeautifulSoup
from getvalue.GetValue import getval

def get_login_info(pw: str,id: str,ses)->List:
    url_before_login = 'https://clubj.jr-odekake.net/shared/pc/login2.do?JRSSID=0409&RTURL=https%3a%2f%2fwww.jr-odekake.net%2f&NTURL=https%3a%2f%2fe5489.jr-odekake.net%2fe5489%2fcspc%2fCBTopMenuPC'
    res_before_login = ses.get(url_before_login)# 先にログインページにアクセスしてクッキーを保存
    soup_before_login = BeautifulSoup(res_before_login.content,'html.parser')
    url_login_form = soup_before_login.find('form',{'name':'login2Form'})
    url_login = 'https://clubj.jr-odekake.net/shared/pc/'+url_login_form['action']
    
    jrssid = getval(soup_before_login,'input','JRSSID','value')
    rturl = getval(soup_before_login,'input','RTURL','value')
    nturl = getval(soup_before_login,'input','NTURL','value')
    sbmtok = getval(soup_before_login,'input','sbmtok','value')
    
    login_info = {
        'id':id,
        'password':pw,
        'JRSSID':jrssid,
        'RTURL':rturl,
        'NTURL':nturl,
        'LIVSID':'',
        'LIVVID':'',
        'sbmtok':sbmtok#文字コード大丈夫か?
    }
    return url_login,login_info