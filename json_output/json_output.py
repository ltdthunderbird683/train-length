#coding:utf-8
import json
train_kind = {
    '特急サンダーバード':'ThunderBird',
    '特急しらさぎ':'Shirasagi',
    '特急くろしお':'Kuroshio',
    '特急はまかぜ':'Hamakaze',
    '特急（ワイドビュー）しなの':'Shinano',
    '特急やくも':'Yakumo'
    }
    
def json_output(dict:dict,expressName:str,date:str):
    exName = train_kind[expressName]
    fileName = str(exName)+date+'.json'
    with open(fileName,'w') as f:
        json.dump(dict,f,ensure_ascii=False,indent=4, sort_keys=False,separators=(',',':'))
        
def dict_maker(expressName:str,date:str,no_length_list:list):
    trainDict = {
    'name':expressName,
    'date':date,
    'info':no_length_list
        }
    dict_for_json = {
    'train':trainDict
    }
    
    return dict_for_json







