#coding:utf-8

def getval(s,tag: str,name: str,element: str)->str:#s(oup)中のnameという名前のtagのelement(要素)の値を取得
    try:
        val = s.find(tag,{'name':name})
        val = val[element]
    except KeyError:
        val = ''
    return val