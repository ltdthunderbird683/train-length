#coding:utf-8

import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin 

def getval(s,tag,name,element):#s(oup)中のnameという名前のtagのelement(要素)の値を取得
    try:
        val = s.find(tag,{'name':name})
        val = val[element]
    except KeyError:
        val = ''
    return val
    
def absurl(relurl):
    u = 'https://e5489.jr-odekake.net'+relurl
    return u
    
def MinuteModify(minu):
    if minu[0] == '0':
        mod_min = minu[1]
    
    mod_min = float(minu)
    mod_min = mod_min/5
    mod_min = int(mod_min)*5
    if mod_min<10:
        mod_min = '0'+str(mod_min)
    else:
        mod_min = str(mod_min)
    return mod_min
    
    




ID = '360811563814'
PW = '14801480Ss'

session = requests.session()
res = session.get('https://e5489.jr-odekake.net/e5489/cspc/CBTopMenuPC?screenId=CP6100&businessType=10&FMLOGIN=1')

# 先にログインページにアクセスしてクッキーを保存する
url_login = 'https://clubj.jr-odekake.net/shared/pc/login2.do?JRSSID=0409&RTURL=https%3a%2f%2fwww.jr-odekake.net%2f&NTURL=https%3a%2f%2fe5489.jr-odekake.net%2fe5489%2fcspc%2fCBTopMenuPC'
res = session.get(url_login)
soup = BeautifulSoup(res.content,'html.parser')
url_login = soup.find('form',{'name':'login2Form'})
url_login = url_login['action']
url_login = 'https://clubj.jr-odekake.net/shared/pc/'+url_login

jrssid = soup.find('input',{'name':'JRSSID'})
jrssid = jrssid['value']
rturl = soup.find('input',{'name':'RTURL'})
rturl = rturl['value']
nturl = soup.find('input',{'name':'NTURL'})
nturl = nturl['value']
sbmtok = soup.find('input',{'name':'sbmtok'})
sbmtok = sbmtok['value']

login_info = {
    'id':ID,
    'password':PW,
    'JRSSID':jrssid,
    'RTURL':rturl,
    'NTURL':nturl,
    'LIVSID':'',
    'LIVVID':'',
    'sbmtok':sbmtok#文字コード大丈夫か?
}

res = session.post(url_login, data=login_info)
res.raise_for_status()

soup = BeautifulSoup(res.content,'html.parser')
iframe = soup.select('iframe')
iframe1 = iframe[0]
url_iframe1 = iframe1['src']
url_iframe1 = 'https://clubj.jr-odekake.net/shared/pc/'+url_iframe1
iframe2 = iframe[1]
url_iframe2 = iframe2['src']
#iframe内のurlをget(これでcookieが取得できた)

res=session.get(url_iframe1)
soup = BeautifulSoup(res.content,'html.parser')
p_url = soup.find('input',{'name':'p_url01'})
p_url = p_url['value']

res=session.get(url_iframe2)


#トップページurlをget
res=session.get(p_url)

#列車の出発時間をループ前に指定
nextHour = '04'
nextMinute = '30'
fin = 1
#以下をループ
while fin > 0:
    
    
    soup = BeautifulSoup(res.content,'html.parser')
    
    
    form_hokuriku = soup.find('form',{'name':'formTrainSimpleEntry2'})
    
    
    businessType = getval(form_hokuriku,'input','businessType','value')
    screenId = getval(form_hokuriku,'input','screenId','value')
    simpleDispFlag = getval(form_hokuriku,'input','simpleDispFlag','value')
    
    url_hokuriku = getval(soup,'form','formTrainSimpleEntry2','action')
    url_hokuriku = absurl(url_hokuriku)
    
    
    #formTrainSimpleEntry2という名前のformに以下の情報をpost
    info_entry2 = {
        'businessType':businessType,
        'screenId':screenId,
        'simpleDispFlag':simpleDispFlag
    }
    
    
    url_hokuriku='https://e5489.jr-odekake.net/e5489/cspc/CBTrainSimpleEntryPC'
    res = session.post(url_hokuriku,data=info_entry2)
    soup = BeautifulSoup(res.content,'html.parser')
    
    
    
    url_search = getval(soup,'form','formMain','action')
    url_search = absurl(url_search)
    
    formMain = soup.find('form',{'name':'formMain'})
    
    businessType = getval(formMain,'input','businessType','value')
    screenId = getval(formMain,'input','screenId','value')
    inputUniqueDepartSt = getval(formMain,'input','inputUniqueDepartSt','value')
    inputUniqueArriveSt = getval(formMain,'input','inputUniqueArriveSt','value')
    inputResultCount = getval(formMain,'input','inputResultCount','value')
    inputSinkansen = getval(formMain,'input','inputSinkansen','value')
    inputNotSinkansen = getval(formMain,'input','inputNotSinkansen','value')
    inputselectdirectflg = getval(formMain,'input','inputselectdirectflg','value')
    simpleDispFlag = getval(formMain,'input','simpleDispFlag','value')
    inputSearchType = getval(formMain,'input','inputSearchType','value')
    inputHour = nextHour
    inputMinute = nextMinute
    
    
    
    #駅、日時などの情報をpost
    url_search = 'https://e5489.jr-odekake.net/e5489/cspc/CBRouteGoodsPC'
    train_info = {
        'businessType':businessType,
        'screenId':screenId,
        'inputUniqueDepartSt':inputUniqueDepartSt,
        'inputUniqueArriveSt':inputUniqueArriveSt,
        'inputResultCount':inputResultCount,
        'inputSinkansen':inputSinkansen,
        'inputNotSinkansen':inputNotSinkansen,
        'inputselectdirectflg':inputselectdirectflg,
        'simpleDispFlag':simpleDispFlag,
        'inputSearchType':inputSearchType,
        'inputDepartStName':'大阪_1'.encode('Shift_JIS'),#乗車駅
        'inputArriveStName':'金沢_0'.encode('Shift_JIS'),#発車駅
        'inputDate':'20191126',#乗車日
        'inputHour':inputHour,#乗車時刻
        'inputMinute':inputMinute,#乗車時刻
        'inputType':'0',
        'inputSinkansenTrain':'0001'
    }
    
    res = session.post(url_search,data=train_info)
    soup = BeautifulSoup(res.content,'html.parser')
    
    #大人料金、子供料金を取得（時期によって変わる）
    money = soup.find('a',href='#route1-ticket-3-1')
    money = money.find('span')
    money = money.text
    money_list = money.split('(')
    for i in range(len(money_list)):
        money_list[i] = money_list[i].strip().strip(')')
    opLogAdultsPrice1 = money_list[0]
    opLogChildsPrice1 = money_list[1]
    
    #1番上の特急名を取得
    TrainName = soup.select('.route-train-list-2__train-name')
    TrainName = TrainName[0].contents
    TrainName = TrainName[0].strip()
    
    #次に発車する列車の時刻を取得
    depart_list = soup.find_all('ol',class_='route-train-list-2')
    try:
        next_depart_time = depart_list[1].find('li').contents
        next_depart_time = next_depart_time[0].split('\n')
        next_depart_time = next_depart_time[2].strip('(').strip('発)').split(':')
        nextHour = next_depart_time[0]
        nextMinute = MinuteModify(next_depart_time[1])
        fin = 1
    except IndexError:
        fin = 0
    
    
    
    
    #messageNoの値が毎回かわるのでそれを取得するコードを書く
    
    
    section = soup.find('section',id='route1-ticket-3-1')
    businessType = getval(section,'input','businessType','value')
    screenId = getval(section,'input','screenId','value')
    #inquiryNo = getval(section,'input','inquiryNo','value')#jsの関数
    screenSequenceNo = getval(section,'input','screenSequenceNo','value')
    mN = getval(section,'input','messageNo','value')
    choiceNo = getval(section,'input','choiceNo','value')
    sewerage1_1 = getval(section,'input','sewerage1_1','value')
    
    
    #1106のサンダーバードn号の普通車禁煙を選択
    url_smoke = 'https://e5489.jr-odekake.net/e5489/cspc/CBNumberSeatRouteGoodsPC'
    url_smoke2 = 'https://e5489.jr-odekake.net/e5489/cspc/CBDayTimeArriveSelRtnRouteGoodsPC'
    smoke_info = {
        'businessType':businessType,
        'screenId':screenId,
        'serviceCd':'',#jsの関数の引数
        'inquiryNo':'01',#jsの関数の引数
        'screenSequenceNo':screenSequenceNo,
        'messageNo':mN,#アクセス毎に変わるので、ここを毎回取得する設定
        'choiceNo':choiceNo,
        'sewerage1_1':sewerage1_1,
        'opLogTrainName1':TrainName.encode('Shift_JIS') ,#列車名
        'opLogSewerage1':'普通車指定席'.encode('Shift_JIS'),#座席の種類
        'opLogEquipment1':'普通'.encode('Shift_JIS'),#車両のグレード,普通グリーングランクラス
        'opLogIncidentalEquipment1':'普通禁煙'.encode('Shift_JIS'),#車両のグレードとタバコ
        'opLogTicketName1':'通常のきっぷ'.encode('Shift_JIS'),#切符の種類
        'opLogAdultsPrice1':opLogAdultsPrice1,#大人の料金、時期によって変わる
        'opLogChildsPrice1':opLogChildsPrice1#子ども料金、時期によって変わる
    }
    
    res = session.post(url_smoke,data=smoke_info)
    soup = BeautifulSoup(res.content,'html.parser')
    
    
    
    formMain = soup.find('form',{'name':'formMain'})
    final_url = formMain['action']
    final_url = absurl(final_url)
    
    
    businessType = getval(formMain,'input','businessType','value')
    screenId = getval(formMain,'input','screenId','value')
    messageNo = getval(formMain,'input','messageNo','value')
    choiceNo = getval(formMain,'input','choiceNo','value')
    discPlanKbn = getval(formMain,'input','discPlanKbn','value')#########
    branchTrainName1 = getval(formMain,'input','branchTrainName1','value')
    branchSewerageType1 = getval(formMain,'input','branchSewerageType1','value')
    branchSmokeType1 = getval(formMain,'input','branchSmokeType1','value')
    
    
    print(TrainName)
    
    final_info = {
        'AdultsNum':'1',#大人の人数
        'ChildsNum':'0',#子どもの人数
        'seatPosition1':'21',#自分で選択
        'ticketType':'1',#片道乗車券
        'businessType':businessType,
        'screenId':screenId,
        'messageNo':messageNo,
        'choiceNo':choiceNo,
        'discPlanKbn':discPlanKbn,
        'branchTrainName1':branchTrainName1.encode('Shift_JIS'),
        'branchSewerageType1':branchSewerageType1,
        'branchSmokeType1':branchSmokeType1
        }
    
    res = session.post(final_url,data=final_info)
    soup = BeautifulSoup(res.content,'html.parser')
    ryousu_list = soup.select('.seat-info-car-navigation__number')
    ryousu = len(ryousu_list)
    ryousu = str(ryousu)+'両'
    print(ryousu)
    if fin == 0:
        break
    
    url_top = getval(soup,'form','formTop','action')
    url_top = absurl(url_top)
    
    formTop = soup.find('form',{'name':'formTop'})
    businessType = getval(formTop,'input','businessType','value')
    screenId = getval(formTop,'input','screenId','value')
    
    top_info = {'businessType':businessType,'screenId':screenId}
    
    res = session.post(url_top,data=top_info)



