#coding:utf-8

import json
import time
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin 
from typing import Tuple,List,Dict
from conf_loader.login_info_loader import id_and_password_loader
from TimeModify.minutes_modify import MinuteModify
from getvalue.GetValue import getval
from logininfo.logininfo import get_login_info
from go_to_reservation.reservationPage import go_to_reservation
from train_info_maker.train_info_maker import TrainInfo_constant
from train_info_maker.train_info_maker import TrainInfo
from train_info_maker.train_info_maker import DA_Station
from train_info_maker.train_info_maker import search_DA_Station_value_from_webpage
from train_info_maker.train_info_maker import getval_of_TrainInfo
from number_name_separeter.separete import number_name_separete
from json_output.json_output import json_output
from json_output.json_output import dict_maker
no_length_list = []
    
session = requests.session()
date = '20200205'


ID,PW = id_and_password_loader('idpw.txt')

url_login,login_info = get_login_info(PW,ID,session)

res_after_login = session.post(url_login, data=login_info)

soup_after_login = BeautifulSoup(res_after_login.content,'html.parser')
iframe = soup_after_login.select('iframe')
iframe1 = iframe[0]
url_iframe1 = 'https://clubj.jr-odekake.net/shared/pc/'+iframe1['src']

#iframe内のurlをget(これでcookieが取得できる)
res_iframe1 = session.get(url_iframe1)
soup_iframe1 = BeautifulSoup(res_iframe1.content,'html.parser')
p_url = getval(soup_iframe1,'input','p_url01','value')

iframe2 = iframe[1]
url_iframe2 = iframe2['src']
res_iframe2=session.get(url_iframe2)


#トップページurlをget
res_top=session.get(p_url)
url_base = 'https://e5489.jr-odekake.net'



inputHour = '04'
inputMinute = '30'
zero_count = 0
train_info_constant = TrainInfo_constant()
dstast = DA_Station()

#####################以下ループ######################
while True:
    time.sleep(7)
    #予約ページへ遷移
    url_reserve,info_entry = go_to_reservation(res_top)
    res = session.post(url_reserve,data = info_entry)
    soup = BeautifulSoup(res.content,'html.parser')
    
    #列車一覧へ遷移
    formMain = soup.find('form',{'name':'formMain'})
    formMain_url,train_info = getval_of_TrainInfo(train_info_constant,formMain,url_base,dstast,date,inputHour,inputMinute)
    train_info_dict = train_info.getDict()
    
    res = session.post(formMain_url,data=train_info_dict)
    soup = BeautifulSoup(res.content,'html.parser')
    
    
    error = soup.find('div',class_='error-message')
    if error != None:
        break
    
    ####################駅名確認がある場合########################
    step = soup.find('ol',class_='step-list')
    selected_step_tag = step.find('li',class_='selected')
    selected_step_text = selected_step_tag.text
    if selected_step_text == '条件入力':
        dstast2 = search_DA_Station_value_from_webpage(soup)
        formMain = soup.find('form',{'name':'formMain'})
        formMain_url2,train_info2 = getval_of_TrainInfo(train_info_constant,formMain,url_base,dstast2,date,inputHour,inputMinute)
        train_info_dict2 = train_info2.getDict_check()
        res = session.post(formMain_url,data=train_info_dict2)
        soup = BeautifulSoup(res.content,'html.parser')
        
        error = soup.find('div',class_='error-message')
        if error != None:
            break
        
    ############################################
    
    
    #検索した時刻より後で一番早く発車する特急を探す
    
    expressGet = 1
    while expressGet > 0:
    
    
        trainType_list=[]
        
        try:
            formMain1 = soup.find('form',{'name':'formMain1'})
            trainType = formMain1.find('div',class_='route-train-list__train-name').get('class')
            trainType_list.append(trainType[1])
        except AttributeError:
            break
        
        try:
            formMain2 = soup.find('form',{'name':'formMain2'})
            trainType = formMain2.find('div',class_='route-train-list__train-name').get('class')
            trainType_list.append(trainType[1])
        except AttributeError:
            break
            
        try:
            formMain3 = soup.find('form',{'name':'formMain3'})
            trainType = formMain3.find('div',class_='route-train-list__train-name').get('class')
            trainType_list.append(trainType[1])
        except AttributeError:
            break
        
        if trainType_list == []:
            break
        
        
        routeNumber = 0
        for i in range(len(trainType_list)):
            routeNumber += 1
            if trainType_list[i] == 'express':
                expressGet = 0
                break
            
            if routeNumber == 3:#次のページへ遷移
                formAfterResearch = soup.find('form',{'name':'formAfterReSearch'})
                next_url = formAfterResearch['action']
                next_url = urljoin(url_base,next_url)
                screenId = getval(formAfterResearch,'input','screenId','value')
                businessType = getval(formAfterResearch,'input','businessType','value')
                inputReSearchType = getval(formAfterResearch,'input','inputReSearchType','value')
                inputReSearchTime = getval(formAfterResearch,'input','inputReSearchTime','value')
                
                next_info = {
                    'screenId':screenId,
                    'businessType':businessType,
                    'inputReSearchType':inputReSearchType,
                    'inputReSearchTime':inputReSearchTime
                }
                
                res = session.post(next_url,data=next_info)
                soup = BeautifulSoup(res.content,'html.parser')
        
    #選んだ経路番号によってpostする変数名や値が変わるので調整
    choiceNo = '0'+str(routeNumber)
    sewerage = 'sewerage'+str(routeNumber)+'_1'
    tagName = 'formMain'+str(routeNumber)
    
    formMain = soup.find('form',{'name':tagName})
    if formMain == None:
        break
    #列車名を取得
    TrainName = formMain.find('div', class_ = 'route-train-list__train-name')
    TrainName = TrainName.p.contents[0]
    
    #列車の発車時間の取得
    TrainTime = formMain.find('div',class_='route-train-list__header-inner')
    TrainTime = TrainTime.find('p',class_='route-train-list__time').contents[0]
    TrainTime = TrainTime.replace(' ','').strip('発').split(':')
    hour = TrainTime[0]
    minute = TrainTime[1]
    
    #切符選択ページへ遷移
    url_formMain1 = formMain['action']
    url_formMain1 = urljoin(url_base,url_formMain1)
    
    
    messageNo = getval(formMain,'input','messageNo','value')
    screenId = getval(formMain,'input','screenId','value')
    businessType = getval(formMain,'input','businessType','value')
    
    formMain1_info = {
        'messageNo':messageNo,
        'choiceNo':choiceNo,# 経路nを選ぶと値は0nになる
        'screenId':screenId,
        'businessType':businessType,
        sewerage:'3010000',#sewarage n_1 nは選んだ経路の番号
        'opLogTrainName1':TrainName.encode('Shift_JIS'),
        'opLogSewerage1':'普通車指定席'.encode('Shift_JIS'),
        'opLogEquipment1':'普通'.encode('Shift_JIS')
    }
    
    res = session.post(url_formMain1,data = formMain1_info)
    soup = BeautifulSoup(res.content,'html.parser')
    
    #指定席が満席、指定席の設定がないとerrorページへ飛ぶのでその対策
    error = soup.find('div',class_='error-message')
    if error != None:
        print(TrainName)
        print('不明')
        formTop = soup.find('form',{'name':'formTop'})
        url_top = formTop['action']
        url_top = urljoin(url_base,url_top)
        businessType = getval(formTop,'input','businessType','value')
        screenId = getval(formTop,'input','screenId','value')
        top_info = {'businessType':businessType,'screenId':screenId}
        res = session.post(url_top,data=top_info)
        inputHour,inputMinute = MinuteModify(hour,minute)
        
        
        continue
    
    seat_url = 'e5489/cspc/CBNumberSeatPC'
    seat_url = urljoin(url_base,seat_url)
    
    #大人料金、子供料金を取得
    AdultMoney = soup.find('strong',class_='ticket-type-table__total-price')
    AdultMoney = AdultMoney.contents[0]
    ChildMoney = AdultMoney.replace(",",'').strip('円')
    ChildMoney = float(ChildMoney)
    ChildMoney = int(ChildMoney/20)*10
    ChildMoney = str(float(ChildMoney/1000))
    ChildMoney = ChildMoney.replace('.',',')+'00円'
    
    
    #乗車券・人数選択ページへ遷移
    formCommon = soup.find('form',{'name':'formCommon'})
    businessType = getval(formCommon,'input','businessType','value')
    screenId = getval(formCommon,'input','screenId','value')
    messageNo = getval(formCommon,'input','messageNo','value')
    screenSequenceNo = getval(formCommon,'input','screenSequenceNo','value')
    choiceNo = getval(formCommon,'input','choiceNo','value')
    #serviceCd = getval(formCommon,'input','serviceCd','value')
    #inquiryNo = getval(formCommon,'input','inquiryNo','value')
    opLogTicketName = soup.find('a',href='#ticket-1').contents[0]
    opLogAdultsSummary = AdultMoney
    opLogChildsSummary = ChildMoney
    opLogAmountCoupon = AdultMoney.strip('円')+' '+'+'+' '+ChildMoney
    
    common_info = {
        'businessType':businessType,
        'screenId':screenId,
        'messageNo':messageNo,
        'screenSequenceNo':screenSequenceNo,
        'choiceNo':choiceNo,
        'serviceCd':'',#jsの引数
        'inquiryNo':'01',#通常の切符なら基本01でOK?
        'opLogTicketName':opLogTicketName.encode('Shift_JIS'),
        'opLogAdultsSummary':opLogAdultsSummary.encode('Shift_JIS'),
        'opLogChildsSummary':opLogChildsSummary.encode('Shift_JIS'),
        'opLogAmountCoupon':opLogAmountCoupon.encode('Shift_JIS')
    }
    
    
    res = session.post(seat_url,data=common_info)
    soup = BeautifulSoup(res.content,'html.parser')
        
        
    #座席確認ページへ遷移    
    formMain = soup.find('form',{'name':'formMain'})
    error = soup.find('div',class_='error-message') #満席の場合、次のページへ遷移がうまくできない
    if error != None:
        print(TrainName+'\n'+'満席やで')
        inputHour,inputMinute = MinuteModify(hour,minute)
        continue
    final_url = formMain['action']
    final_url = urljoin(url_base,final_url)
        
        
    businessType = getval(formMain,'input','businessType','value')
    screenId = getval(formMain,'input','screenId','value')
    messageNo = getval(formMain,'input','messageNo','value')
    choiceNo = getval(formMain,'input','choiceNo','value')
    discPlanKbn = getval(formMain,'input','discPlanKbn','value')
    branchTrainName1 = getval(formMain,'input','branchTrainName1','value')
    branchSewerageType1 = getval(formMain,'input','branchSewerageType1','value')
    branchSmokeType1 = getval(formMain,'input','branchSmokeType1','value')
        
    final_info = {
        'AdultsNum':'1',#大人の人数
        'ChildsNum':'0',#子どもの人数
        'seatPosition1':'21',#自分で選択
        'ticketType':'1',#片道乗車券
        'businessType':businessType,
        'screenId':screenId,
        'messageNo':messageNo,
        'choiceNo':choiceNo,
        'discPlanKbn':discPlanKbn,
        'branchTrainName1':branchTrainName1.encode('Shift_JIS'),
        'branchSewerageType1':branchSewerageType1,
        'branchSmokeType1':branchSmokeType1
        }
    
    res = session.post(final_url,data=final_info)
    soup = BeautifulSoup(res.content,'html.parser')
    ryousu_list = soup.select('.seat-info-car-navigation__number')
    ryousu = len(ryousu_list)
    #time.sleep(10)
    
    #topページへ戻る
    formTop = soup.find('form',{'name':'formTop'})
    url_top = formTop['action']
    url_top = urljoin(url_base,url_top)
    businessType = getval(formTop,'input','businessType','value')
    screenId = getval(formTop,'input','screenId','value')
    top_info = {'businessType':businessType,'screenId':screenId}
    res = session.post(url_top,data=top_info)
    
    #両数が０になるエラーが発生しがちなので、両数が０になったらやり直すように設定
    #サンライズなどの特殊な車両の場合、両数が０になってしまうので、
    #zero_errorが3回発生すれば次の特急の情報を取得するよう設定
    if ryousu == 0:
        print('zero_error')
        zero_count += 1
        if zero_count == 3:
            zero_count = 0
            print(TrainName+'不明')
            inputHour,inputMinute = MinuteModify(hour,minute)
            continue
        continue
    else:
        zero_count = 0
    

    expressNumber,expressName = number_name_separete(TrainName)
        
    if expressNumber != 'error':
        no_length_dict = {'no':expressNumber,'length':ryousu}
        no_length_list.append(no_length_dict)
    #次の検索時刻を設定
    inputHour,inputMinute = MinuteModify(hour,minute)
    
dict_for_json = dict_maker(expressName,date,no_length_list)
json_output(dict_for_json,expressName,date)

