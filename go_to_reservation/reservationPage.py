#coding:utf-8
from bs4 import BeautifulSoup
from getvalue.GetValue import getval
from urllib.parse import urljoin 

def go_to_reservation(res_top):
    url_base = 'https://e5489.jr-odekake.net'
    soup_top = BeautifulSoup(res_top.content,'html.parser')
    formTrainEntry = soup_top.find('form',{'name':'formTrainEntry'})
    url_reserve = urljoin(url_base,formTrainEntry['action'])
    businessType = getval(formTrainEntry,'input','businessType','value')
    screenId = getval(formTrainEntry,'input','screenId','value')
    info_entry = {'businessType':businessType,'screenId':screenId}
    
    return url_reserve,info_entry
    