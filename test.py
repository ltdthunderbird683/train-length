#coding:utf-8

class Test:
    def __init__(self,a=3,b=4):
        self.a = a
        self.b = b
        
    def product(self):
        return self.a*self.b
        
    def valChange(self):
        newClass = Test(a=self.a,b=self.b)
        return newClass
        
A = Test(3,4)
print(A.product())

A.a = 4
B = A.valChange()
print(B.product())


    