#!/bin/bash

for i in `seq 19 30 `
do
  date=$(python hiduke.py ${i})

  python former_allexp.py 大阪 金沢 ${date} > ryousu/ThunderBird_down.txt
  python former_allexp.py 金沢 大阪 ${date} > ryousu/ThunderBird_up.txt
  python former_allexp.py 米原 金沢 ${date} > ryousu/Shirasagi_down.txt
  python former_allexp.py 金沢 米原 ${date} > ryousu/Shirasagi_up.txt
  python former_allexp.py 新大阪 和歌山 ${date} > ryousu/Kuroshio_hanwa_down.txt
  python former_allexp.py 和歌山 新大阪 ${date} > ryousu/Kuroshio_hanwa_up.txt
  python former_allexp.py 新大阪 紀伊田辺 ${date} > ryousu/Kuroshio_kinokuni_down.txt
  python former_allexp.py 紀伊田辺 新大阪  ${date} > ryousu/Kuroshio_kinokuni_up.txt
  python former_allexp.py 姫路 城崎温泉 ${date} > ryousu/Hamakaze_down.txt
  python former_allexp.py 城崎温泉 姫路 ${date} > ryousu/Hamakaze_up.txt
  python former_allexp.py 松本 長野 ${date} > ryousu/Shinano_down.txt
  python former_allexp.py 長野 松本 ${date} > ryousu/Shinano_up.txt
  python Yakumo.py 岡山 出雲市 ${date} > ryousu/Yakumo_down.txt
  python Yakumo.py 出雲市 岡山 ${date} > ryousu/Yakumo_up.txt

  python hatena.py ${date}
done